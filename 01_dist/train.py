#!/usr/bin/env python

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import argparse
from matplotlib import pyplot as plt


# Arguments Parser
parser = argparse.ArgumentParser(description='Generative Adversaral Network Training for Gaussian Distribution')
parser.add_argument('--epochs', type=int, default=5000, metavar='X', help='number of epochs')
parser.add_argument("--lr", type=float, default=1e-3, metavar='X', help='learning rate')
parser.add_argument('--nbr_samples', type=int, default=500, metavar='X', help='number of samples')
parser.add_argument('--d_steps', type=int, default=20, metavar='X', help='number of steps for dis')
parser.add_argument('--g_steps', type=int, default=20, metavar='X', help='number of steps for gen')
parser.add_argument('--print_every', type=int, default=500, metavar='X', help='steps to print')

args = parser.parse_args()

 
# ##### DATA: Target data and generator input data
def get_distribution_sampler(mu, sigma):
    return lambda n: torch.Tensor(np.random.normal(mu, sigma, (1, n)))


def get_generator_input_sampler():
    return lambda m, n: torch.rand(m, n)


# ##### MODELS: Generator model and discriminator model
class Generator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, f):
        super(Generator, self).__init__()
        self.map1 = nn.Linear(input_size, hidden_size)
        self.map2 = nn.Linear(hidden_size, hidden_size)
        self.map3 = nn.Linear(hidden_size, output_size)
        self.f = f

    def forward(self, x):
        x = self.map1(x)
        x = self.f(x)
        x = self.map2(x)
        x = self.f(x)
        x = self.map3(x)
        return x


class Discriminator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, f):
        super(Discriminator, self).__init__()
        self.map1 = nn.Linear(input_size, hidden_size)
        self.map2 = nn.Linear(hidden_size, hidden_size)
        self.map3 = nn.Linear(hidden_size, output_size)
        self.f = f

    def forward(self, x):
        x = self.f(self.map1(x))
        x = self.f(self.map2(x))
        return self.f(self.map3(x))


def setup_model():
    dis = Discriminator(input_size=args.nbr_samples, hidden_size=10, output_size=1, f=torch.sigmoid)
    gen = Generator(input_size=args.nbr_samples, hidden_size=5, output_size=args.nbr_samples, f=torch.tanh)

    return dis, gen 


def setup_data():
    data_mean = 0 # 4 
    data_strdev = 1 # 1.25
    d_sampler = get_distribution_sampler(data_mean, data_strdev)
    gi_sampler = get_generator_input_sampler()

    return d_sampler, gi_sampler


def draw_dist(data, title=None):
    data = data.numpy()
    data = data.reshape(data.size)
    plt.hist(data, bins=int(data.size / 10)) 
    plt.xlabel('Value')
    plt.ylabel('Count')
    plt.title(title) if title is not None else plt.title('Histogram of a Distribution')
    plt.grid(True)
    plt.show()


def iter_dis(dis, gen, d_sampler, gi_sampler, d_optimizer, loss_func):
    dis.zero_grad()
    for i in range(args.d_steps):
        # train on real data
        real_data = d_sampler(args.nbr_samples) # ground-truth
        real_pred = dis(real_data) # descision
        real_loss = loss_func(real_pred, torch.ones_like(real_pred)) 
        real_loss.backward() 

        # train on fake data
        gen_input = gi_sampler(1, args.nbr_samples) # random input noise
        fake_data = gen(gen_input).detach() # fake data from generator
        fake_pred = dis(fake_data) # descision
        fake_loss = loss_func(fake_pred, torch.zeros_like(fake_pred))
        fake_loss.backward()
        d_optimizer.step()

        #draw_dist(real_data, "REAL")
        #draw_dist(fake_data, "FAKE")

        return real_loss.data.item(), fake_loss.data.item()


def iter_gen(dis, gen, g_optimizer, gi_sampler, loss_func):
    gen.zero_grad()
    for i in range(args.g_steps):
        # train on disciminators response
        gen_input = gi_sampler(1, args.nbr_samples) # random input noise
        fake_data = gen(gen_input)
        pred = dis(fake_data)
        loss = loss_func(pred, torch.ones_like(pred))
        loss.backward()
        g_optimizer.step()

        return loss.data.item()
        

def train():
    d_sampler, gi_sampler = setup_data()
    dis, gen = setup_model()
    
    loss_func = nn.BCELoss()
    d_optimizer = optim.SGD(dis.parameters(), lr=args.lr, momentum=0.9)
    g_optimizer = optim.SGD(gen.parameters(), lr=args.lr, momentum=0.9)

    for i in range(args.epochs):
        d_real, d_fake = iter_dis(dis, gen, d_sampler, gi_sampler, d_optimizer, loss_func)
        g_loss = iter_gen(dis, gen, g_optimizer, gi_sampler, loss_func)
        
        if i % args.print_every == 0 and i:
            print(f"Epoch {i}")
            print(f"    Dis-Loss: {round(d_real, 5)} (real), {round(d_fake, 5)} (fake)")
            print(f"    Gen-Loss: {round(g_loss, 5)}")
    
    draw_dist(gen(gi_sampler(1, args.nbr_samples)).detach(), "Final Generated Distribution")


train()

